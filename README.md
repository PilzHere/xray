XRay is an application used to see what's under the hood of your computer.


It is FREE to use, change and distribute!



Xray can read information from:

Motherboard, BIOS, Processor, Memory, Operating System, Mounted Drives & Partitions, Graphics Processor, Network Interfaces and USB Devices



Tested platforms:

Windows � Linux.

But should also work on Mac OS X � Unix (Solaris, FreeBSD).


JAR requires Java installed to run.


JRE built with: jre1.8.0_151.


Dependencies: JavaFX SDK, Oshi-core-3.4.4 (with respective dependencies), LWJGL 3.1.5 build 1 (Preset: Minimal OpenGL).


Download JAR or repository from: https://bitbucket.org/PilzHere/xray/downloads/
